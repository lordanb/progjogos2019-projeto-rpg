
return {
    name = 'Fireball',
    condition = nil,
    cost = 10,
    effect = {stat = 'hp', value = -25, time = -1},
    message = 'The battlefield is consumed in flames'
}