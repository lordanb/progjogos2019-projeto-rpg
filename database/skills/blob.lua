
return {
    name = 'Blob Strike',
    condition = nil,
    cost = 5,
    effect = {stat = 'hp', value = -15, time = -1},
    message = 'A slimey substance strikes'
}