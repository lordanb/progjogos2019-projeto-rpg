
return {
    name = 'Piercing Arrow',
    condition = nil,
    cost = 5,
    effect = {stat = 'hp', value = -15, time = -1},
    message = 'A righteous arrow pierces the enemy'
}