
return {
    name = 'Heal',
    condition = nil,
    cost = 10,
    effect = {stat = 'hp', value = 25, time = -1},
    message = 'A healing aura surrounds you'
}