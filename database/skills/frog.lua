
return {
    name = 'Frog Incantation',
    cost = 10,
    condition = {name = 'Frog', new_sprite = 'frog', time = 1, actions = {'Fight'}},
    effect = nil,
    message = 'The spell turns them into a frog!'
}