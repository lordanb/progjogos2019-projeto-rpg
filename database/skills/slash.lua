
return {
    name = 'Rusty Slash',
    condition = nil,
    cost = 5,
    effect = {stat = 'hp', value = -15, time = -1},
    message = 'A worn out blade slashes through the enemy'
}