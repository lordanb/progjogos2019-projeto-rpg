
return {
    name = 'Piercing Stare',
    condition = {name = 'Paralysis', new_sprite = nil, 
    	time = 3, actions = {'Fight', 'Item', 'Skill', 'Run'},
    	dist = nil},
    cost = 10,
    effect = nil,
    message = 'The enemy is paralyzed in fear!'
}