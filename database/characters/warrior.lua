
return {
  name = "Veteran Warrior",
  appearance = 'knight',
  max_hp = 20,
  max_mp = 15,
  attack = 4,
  resistance = 4,
  speed = 5,
  accuracy = 1,
  avoidance = 1,
  critical = 5,
  skills = {'slash'}
}

