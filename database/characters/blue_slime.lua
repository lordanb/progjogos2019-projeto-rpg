
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 16,
  max_mp = 10,
  attack = 7,
  resistance = 1,
  speed = 1,
  accuracy = 8,
  avoidance = 2,
  critical = 15,
  skills = {'blob', 'stare'},
  targets = { 'Veteran Warrior' },
  critical_behaviour = 'item',
  items = { { 'hp_potion', 2 }, { 'mp_potion', 2 } },
  poison_chance = 0.6
}

