
return {
  name = "Recluse Archer",
  appearance = 'archer',
  max_hp = 12,
  max_mp = 20,
  attack = 4,
  resistance = 3,
  speed = 10,
  accuracy = 10,
  avoidance = 10,
  critical = 20,
  skills = {'stare', 'arrow'}
}

