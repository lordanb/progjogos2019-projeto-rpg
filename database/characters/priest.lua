
return {
  name = "Friendly Priest",
  appearance = 'priest',
  max_hp = 8,
  max_mp = 25,
  attack = 4,
  resistance = 2,
  speed = 7,
  accuracy = 10,
  avoidance = 3,
  critical = 5,
  skills = {'fireball', 'frog', 'heal'}
}

