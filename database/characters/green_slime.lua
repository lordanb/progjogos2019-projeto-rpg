
return {
  name = "Green Slime",
  appearance = 'slime',
  max_hp = 10,
  max_mp = 5,
  attack = 5,
  resistance = 1,
  speed = 1,
  accuracy = 6,
  avoidance = 2,
  critical = 5,
  skills = {'blob'},
  targets = { 'Friendly Priest' },
  critical_behaviour = 'item',
  items = { { 'hp_potion', 2 } },
  poinson_chance = 0.4
}

