
return {
  title = 'Slime Infestation',
  party = { 'warrior', 'archer', 'priest' },
  items = {
    {'hp_potion', 3}, {'mp_potion', 3}, {'bottled_warcry', 1}, {'confuse', 1}
  },
  encounters = {
    { 'green_slime', 'blue_slime', 'green_slime' },
    { 'green_slime', 'green_slime', 'green_slime' },
    { 'blue_slime', 'green_slime' }
  }
}