
return {
    name = 'Healthy Potion',
    effect = {stat = 'hp', value = 10, time = -1},
    message = 'Drinking it restored 10 points of health'
}