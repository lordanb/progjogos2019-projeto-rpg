
return {
    name = 'Vitality Potion',
    effect = {stat = 'mp', value = 10, time = -1},
    message = 'Drinking it restored 10 points of energy'
}