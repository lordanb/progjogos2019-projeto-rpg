
return {
    name = 'Confounding Gas',
    condition = {name = 'Confusion', new_sprite = nil, 
    	time = 3, actions = nil, dist = {0.75, 0.15, 0.1}},
    effect = nil,
    message = 'They are now confused!'
}