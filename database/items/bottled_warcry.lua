
return {
    name = 'Bottled War Cry',
    effect = {stat = 'attack', value = 3, time = 2},
    message = 'You hear a growl as the bottle opens and feel inspired'
}