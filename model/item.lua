return function (ruleset)

    local r = ruleset.record
  
    r:new_property('item', { name = '', effect = nil, condition = nil, amount = 0, message = '' })
  
    function ruleset.define:create_item(spec, eff, cond, amount)
      function self.when()
        return true
      end
      function self.apply()
        local e = ruleset:new_entity()
        r:set(e, 'item', { name = spec.name,
          amount = amount, message = spec.message })
        r:set(e, 'item', 'effect', eff)
        r:set(e, 'item', 'condition', cond)
        return e
      end
    end
  
    function ruleset.define:get_name(e)
      function self.when()
        return r:is(e, 'item')
      end
      function self.apply()
        return r:get(e, 'item', 'name')
      end
    end
  
    function ruleset.define:get_effect(e)
      function self.when()
        return r:is(e, 'item')
      end
      function self.apply()
        return r:get(e, 'item', 'effect')
      end
    end
  
    function ruleset.define:get_condition(e)
      function self.when()
        return r:is(e, 'item')
      end
      function self.apply()
        return r:get(e, 'item', 'condition')
      end
    end

    function ruleset.define:get_message(e)
      function self.when()
        return r:is(e, 'item')
      end
      function self.apply()
        return r:get(e, 'item', 'message')
      end
    end

    function ruleset.define:get_amount(e)
      function self.when()
        return r:is(e, 'item')
      end
      function self.apply()
        return r:get(e, 'item', 'amount')
      end
    end

    function ruleset.define:increment_amount(e, increment)
      function self.when()
        return r:is(e, 'item')
      end
      function self.apply()
        return r:set(e, 'item', 'amount', r:get(e, 'item', 'amount') + increment)
      end
    end
  end
  