
return function (ruleset)

  local r = ruleset.record

  r:new_property('character', {
    ally = true, hp = 1, mp = 1, max_hp = 1, max_mp = 1, appearance = '', name = '',
    attack = 1, resistance = 1, speed = 1, accuracy = 1, avoidance = 1, critical = 1,
    buffs = {}, condition = nil, skills = {}, poison_chance = 0, is_poisoned = false
  })

  r:new_property('enemy', {
    targets = {}, critical_behaviour = {}, items = {}
  })

  function ruleset.define:create_character(spec, skills, isAlly)
    function self.when()
      return true
    end
    function self.apply()
      local e = ruleset:new_entity()
      r:set(e, 'character', {
        ally = isAlly, hp = spec.max_hp, mp = spec.max_mp, name = spec.name,
        max_hp = spec.max_hp, max_mp = spec.max_mp, appearance = spec.appearance,
        attack = spec.attack, resistance = spec.resistance, speed = spec.speed, 
        accuracy = spec.accuracy, avoidance = spec.avoidance, critical = spec.critical,
        condition = nil, buffs = {}, poison_chance = spec.poison_chance
      })

      r:set(e, 'character', 'skills', skills)
      if not isAlly then
        r:set(e, 'enemy', {
        targets = spec.targets, critical_behaviour = spec.critical_behaviour,
        items = spec.items})
      end

      return e
    end
  end

  function ruleset.define:get_skills(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'skills')
    end
  end

  function ruleset.define:get_hp(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'hp')
    end
  end

  function ruleset.define:get_max_hp(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'max_hp')
    end
  end

  function ruleset.define:get_mp(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'mp'), r:get(e, 'character', 'max_mp')
    end
  end

  function ruleset.define:get_max_mp(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'max_mp')
    end
  end

  function ruleset.define:get_name(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'name')
    end
  end

  function ruleset.define:get_appearance(e)
    function self.when()
      return r:is(e, 'character') and e.condition ~= nil
        and e.condition:get_sprite() ~= ''
    end
    function self.apply()
      return e.condition:get_sprite()
    end
  end

  function ruleset.define:get_appearance(e)
    function self.when()
      return r:is(e, 'character') and e.condition == nil
        or (e.condition ~= nil and e.condition:get_sprite() == '')
    end
    function self.apply()
      return r:get(e, 'character', 'appearance')
    end
  end

  function ruleset.define:get_attack(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'attack')
    end
  end

  function ruleset.define:get_resistance(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'resistance')
    end
  end

  function ruleset.define:get_speed(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'speed')
    end
  end

  function ruleset.define:get_accuracy(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'accuracy')
    end
  end

  function ruleset.define:get_avoidance(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'avoidance')
    end
  end

  function ruleset.define:get_critical(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'critical')
    end
  end

  function ruleset.define:get_poison_chance(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'poison_chance')
    end
  end

  function ruleset.define:get_condition(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'condition')
    end
  end

  function ruleset.define:get_buffs(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:get(e, 'character', 'buffs')
    end
  end
end