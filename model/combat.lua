
return function (ruleset)

  local r = ruleset.record

  r:new_property('combat')

  function ruleset.define:get_all_alive(type)
    function self.when()
      return true
    end
    function self.apply()
      local all = r:where('character', {ally = type})
      local ret = {}
      for _, char in ipairs(all) do
        if char:get_hp() > 0 then
          table.insert(ret, char)
        end
      end
      return ret
    end
  end

  function ruleset.define:reset_state()
    function self.when()
      return true
    end
    function self.apply()
      local everyone = r:where('character', {})
      for _, char in ipairs(everyone) do
        r:clear_all(char)
      end
    end
  end

  function ruleset.define:attack(attacker, defender)
    function self.when()
      return attacker:get_hp() > 0 and defender:get_hp() > 0 and
        attacker:can('Fight')
    end
    function self.apply()
      local hit = math.max(0, attacker:get_accuracy() - defender:get_avoidance())/10
      local hit_chance = math.random()
      if hit_chance + hit >= 0.5 then
        -- hits the target, but is it critical?
        local crit_chance = math.random()
        local crit = attacker:get_critical()/100
        -- is it poisonous?
        local is_poisonous = math.random() <= attacker:get_poison_chance()
        if crit_chance <= crit then
          -- critical hit causes 5x damage
          local dmg = 5 * math.max(attacker:get_attack() - defender:get_resistance(), 1)
          defender:take_damage(dmg)
          return {"Critical", dmg, is_poisonous}
        end

        local dmg = math.max(attacker:get_attack() - defender:get_resistance(), 0)
        defender:take_damage(dmg)
        return {"Fight", dmg, is_poisonous}
      end
      
      -- misses target
      return {"Miss", 0}
    end
  end

  function ruleset.define:random_action(entity, n)
    function self.when()
      return entity.condition ~= nil and entity.condition:get_dist() ~= nil
    end
    function self.apply()
      local actions = {'Fight', 'Skill', 'Item'}
      local prob_dist = entity.condition:get_dist()
      local action = ''
      local action_rng = math.random()
      local accum = 0.0
      for i, v in ipairs(prob_dist) do
        if action_rng <= (accum + v) then
          action = actions[i]
          break
        end
        accum = accum + v 
      end
      local target_rng = math.random(1,n)
      return {action, target_rng}
    end
  end


  function ruleset.define:use_skill(user, item, target)
    function self.when()
      return user:get_mp() - item:get_amount() > 0 and user:can('Skill')
    end
    function self.apply()
      r:set(user, 'character', 'mp', user:get_mp() - item:get_amount()) --decrease mp
      local effect = item:get_effect()
      target:add_buff(effect)
      local status = item:get_condition()
      target:add_status(status)
      return 'Skill'
    end
  end

  function ruleset.define:use_item(user, item, target)
    function self.when()
      return item:get_amount() > 0 and user:can('Item')
    end
    function self.apply()
      r:set(item, 'item', 'amount', item:get_amount()-1) --decrease item quantity
      local effect = item:get_effect()
      target:add_buff(effect)
      local status = item:get_condition()
      target:add_status(status)
      return 'Item'
    end
  end

  function ruleset.define:take_damage(e, damage)
    function self.when()
      return r:get(e, 'character', 'hp') > 0
    end
    function self.apply()
      return r:set(e, 'character', 'hp', math.max(e.hp - damage, 0))
    end
  end

  function ruleset.define:can(e, action)
    function self.when()
      return r:is(e, 'character') and e:get_hp() > 0
    end
    function self.apply()
      local cond = e:get_condition()
      if cond then
        for _, forbidden_action in ipairs(cond:get_actions()) do
          if action == forbidden_action then
            return false
          end
        end
      end
      return true
    end
  end


end
