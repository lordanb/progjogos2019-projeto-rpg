
return function (ruleset)

  local r = ruleset.record

  r:new_property('enemy', {
    targets = {},
    critical_behaviour = {},
    items = {}
  })

  function ruleset.define:get_targets(e)
    function self.when()
      return r:is(e, 'enemy')
    end
    function self.apply()
      return r:get(e, 'enemy', 'targets')
    end
  end

  function ruleset.define:get_items(e)
    function self.when()
      return r:is(e, 'enemy')
    end
    function self.apply()
      return r:get(e, 'enemy', 'items')
    end
  end

  function ruleset.define:set_items(e, items)
    function self.when()
      return r:is(e, 'enemy')
    end
    function self.apply()
      r:set(e, 'enemy', 'items', items)
    end
  end

  function ruleset.define:get_critical_behaviour(e)
    function self.when()
      return r:is(e, 'enemy')
    end
    function self.apply()
      return r:get(e, 'enemy', 'critical_behaviour')
    end
  end

  function ruleset.define:is_enemy(e)
    function self.when()
      return r:is(e, 'character')
    end
    function self.apply()
      return r:is(e, 'enemy')
    end
  end
end
