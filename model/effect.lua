return function (ruleset)

    local r = ruleset.record
    r:new_property('effect', {stat = 'HP', value = 1, time = -1})

    function ruleset.define:create_effect(spec)
      function self.when()
        return true
      end
      function self.apply()
        local e = ruleset:new_entity()
        r:set(e, 'effect', {stat = spec.stat,
          value = spec.value, time = spec.time})
        return e
      end
    end

    function ruleset.define:get_stat(e)
      function self.when()
        return r:is(e, 'effect')
      end
      function self.apply()
        return r:get(e, 'effect', 'stat')
      end
    end

    function ruleset.define:get_value(e)
      function self.when()
        return r:is(e, 'effect')
      end
      function self.apply()
        return r:get(e, 'effect', 'value')
      end
    end

    function ruleset.define:get_time(e)
      function self.when()
        return r:is(e, 'effect')
      end
      function self.apply()
        return r:get(e, 'effect', 'time')
      end
    end

  function ruleset.define:add_buff(e, buff)
    function self.when()
      return r:get(e, 'character', 'hp') > 0 and buff ~= nil
    end
    function self.apply()
      local statname = buff:get_stat()
      local max = 'max_' .. statname
      local stat = r:get(e, 'character', statname)
      local charMax = r:get(e, 'character', max)
      local value = buff:get_value()
      local new_buffs = e:get_buffs()
      table.insert(new_buffs, buff)
      if charMax ~= nil then --if it has max, cap it there after applying
        r:set(e, 'character', statname,
          math.min(math.max(stat + value, 0), charMax))
      else --just apply, capping at 0
        r:set(e, 'character', statname,
          math.max(stat + value, 0))
      end
      return r:set(e, 'character', 'buffs', new_buffs)
    end
  end

  function ruleset.define:check_buffs(e)
    function self.when()
      return #r:get(e, 'character', 'buffs') > 0
    end
    function self.apply()
      local currentBuffs = e:get_buffs()
      local toRemove = {}
      local i = 1
      local pre_buff_applied = false
      for _, buff in ipairs(currentBuffs) do
        local statname = buff:get_stat()
        local stat = r:get(e, 'character', statname)
        local value = buff:get_value()
        local time = buff:get_time()
        if type(time) ~= 'number' then
          if not pre_buff_applied  then
            r:set(e, 'character', statname, stat-value > 0 and stat-value or 0)
            pre_buff_applied = true
          end
        elseif time > 0 then
          r:set(buff, 'effect', 'time', time-1)
        elseif time == 0 then
          r:set(e, 'character', statname, stat-value)
          table.insert(toRemove, i)
        else
          table.insert(toRemove, i)
        end
        i = i+1
      end
      for _, j in ipairs(toRemove) do
        table.remove(currentBuffs, j)
      end
      return r:set(e, 'character', 'buffs', currentBuffs)
    end
  end

end