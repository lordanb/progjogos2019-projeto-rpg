return function (ruleset)

    local r = ruleset.record

    r:new_property('status', {
      name = '',
      forbidden_actions = {},
      new_sprite = '',
      dist = {},
      time = 3
    })

    function ruleset.define:create_status(spec)
      function self.when()
        return true
      end
      function self.apply()
        local e = ruleset:new_entity()
        r:set(e, 'status', {name = spec.name, 
          message = spec.message, forbidden_actions = spec.actions,
          new_sprite = spec.new_sprite, time = spec.time})
        r:set(e, 'status', 'dist', spec.dist)
        return e
      end
    end

    function ruleset.define:get_name(e)
      function self.when()
        return r:is(e, 'status')
      end
      function self.apply()
        return r:get(e, 'status', 'name')
      end
    end

    function ruleset.define:get_actions(e)
      function self.when()
        return r:is(e, 'status')
      end
      function self.apply()
        return r:get(e, 'status', 'forbidden_actions')
      end
    end

    function ruleset.define:get_dist(e)
      function self.when()
        return r:is(e, 'status')
      end
      function self.apply()
        return r:get(e, 'status', 'dist')
      end
    end

    function ruleset.define:get_sprite(e)
      function self.when()
        return r:is(e, 'status')
      end
      function self.apply()
        return r:get(e, 'status', 'new_sprite')
      end
    end

    function ruleset.define:get_time(e)
      function self.when()
        return r:is(e, 'status')
      end
      function self.apply()
        return r:get(e, 'status', 'time')
      end
    end

  function ruleset.define:add_status(e, status)
    function self.when()
      return r:get(e, 'character', 'hp') > 0
    end
    function self.apply()
      return r:set(e, 'character', 'condition', status)
    end
  end

  function ruleset.define:check_status(e)
    function self.when()
      return r:get(e, 'character', 'condition') ~= nil
    end
    function self.apply()
      local time = e.condition:get_time()
      r:set(e.condition, 'status', 'time', time-1)
      if e.condition:get_time() == 0 then
        return r:set(e, 'character', 'condition', nil)
      end
    end
  end


end