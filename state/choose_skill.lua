
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local ChooseSkillState = require 'common.class' (State)

function ChooseSkillState:_init(stack)
  self:super(stack)
  self.options = nil
  self.character = nil
  self.targets = nil
  self.action = nil
  self.items = nil
  self.menu = nil
end

function ChooseSkillState:enter(params)
  print("entrei no choose skill")
  print(params.action)
  self.character = params.character
  self.action = params.action
  self.targets = params.targets
  self.items = params.items
  if self.action == 'Skill' or self.action == 'Item' then
    self:_show_menu(self.action)
  elseif self.action == 'Fight' then
    return self:push('choose_target', { action = self.action, character = self.character,
      item = nil, targets = self.targets})
  else
    return self:pop(params)
  end
  --self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
end

function ChooseSkillState:_show_menu(type)
  local skills
  if type == 'Item' then
    skills = self:get_item_menu(self.items, true)
  else
    skills = self:get_item_menu(self.character:get_skills(), false)
  end
  self.menu = ListMenu(skills)
  print(skills[1])
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, ((bfbox.top + bfbox.bottom) / 2) - 32)
  self:view():remove('turn_menu')
  self:view():add('turn_menu', self.menu)
end

function ChooseSkillState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function ChooseSkillState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function ChooseSkillState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function ChooseSkillState:resume(params)
  print("voltei pro choose skill")
  if params.target or not params.item then
    return self:pop(params)
  end
  self:enter(params)
end

function ChooseSkillState:get_item_menu(list, item)
  self.option = list
  local prefix = ''
  if item then
    prefix = 'x'
  end
  local names = {}
  for _, e in ipairs(list) do
    table.insert(names, e:get_name() .. '  ' .. prefix .. e:get_amount())
  end
  return names
end

function ChooseSkillState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    local option = self.menu:current_option()
    return self:push('choose_target', { action = self.action, character = self.character,
      item = option, targets = self.targets, items = self.items})
  elseif key == 'escape' then
    return self:pop( { action = self.action, character = self.character, item = nil,
      targets = self.targets, items = self.items } )
  end
end

return ChooseSkillState
