local State = require 'state'

local FollowQuestState = require 'common.class' (State)

function FollowQuestState:_init(stack)
  self:super(stack)
  self.party = nil
  self.encounters = nil
  self.items = nil
  self.next_encounter = nil
end

function FollowQuestState:enter(params)
  local quest = params.quest
  self.encounters = quest.encounters
  self.next_encounter = 1
  self.items = self:create_inventory(quest.items)
  self.party = self:create_characters(quest.party, true)
end

function FollowQuestState:update(_)
  if self.next_encounter <= #self.encounters then
    local encounter = {}
    local encounter_specnames = self.encounters[self.next_encounter]
    self.next_encounter = self.next_encounter + 1

    encounter = self:create_characters(encounter_specnames, false)

    for i, enemy in ipairs(encounter) do
      -- gambiarra para salvar items em character. É possível chamar a engine dentro de um ruleset?
      encounter[i]:set_items(self:create_inventory(encounter[i]:get_items()))
    end

    local params = {
      party = self.party,
      encounter = encounter,
      items = self.items
    }
    return self:push('encounter', params)
  else
    return self:pop()
  end
end

function FollowQuestState:create_characters(list, ally)
  local ret = {}
  for i, character_name in ipairs(list) do
    local character_spec = require('database.characters.' .. character_name)
    local skills = {}
    for _, name in ipairs(character_spec.skills) do
      local item_spec = require('database.skills.' .. name)
      local effect = nil
      local condition = nil
      if item_spec.effect then
        effect = self:engine():create_effect(item_spec.effect)
      elseif item_spec.condition then
        condition = self:engine():create_status(item_spec.condition)
      end
      local skill = self:engine():create_item(item_spec, effect, condition, item_spec.cost)
      table.insert(skills, skill)
    end
    table.insert(ret, self:engine():create_character(character_spec, skills, ally))
  end
  return ret
end

function FollowQuestState:create_inventory(list)
  local ret = {}
  if list then
    for i, item_obj in ipairs(list) do
      local item_spec = require('database.items.' .. item_obj[1])
      local effect = nil
      local condition = nil
      if item_spec.effect then
        effect = self:engine():create_effect(item_spec.effect)
      elseif item_spec.condition then
        condition = self:engine():create_status(item_spec.condition)
      end
      table.insert(ret, self:engine():create_item(item_spec, effect, condition, item_obj[2]))
    end
  end
  return ret
end

return FollowQuestState