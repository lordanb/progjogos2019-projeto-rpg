
local Vec = require 'common.vec'
local MessageBox = require 'view.message_box'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local State = require 'state'

local EncounterState = require 'common.class' (State)

local CHARACTER_GAP = 96
local MESSAGES = {
  Fight = "%s attacked %s, \n causing %s damage!",
  NoFight = "%s tried to attack, \n but they couldn't!",
  Miss = "%s attacked %s, \n but it missed!",
  Critical = "%s attacked %s, \n causing %s of critical damage!",
  Skill = "%s used %s on %s \n %s",
  NoSkill = "%s tried using %s, \n but they were out of energy!",
  Item = "%s used a %s on %s \n %s",
  NoItem = "%s tried using %s, \n but they couldn't!",
  Poison = "%s is poisoned!",
  Died = "%s died from poison!"
}

function EncounterState:_init(stack)
  self:super(stack)
  self.turns = nil
  self.all_entities = nil
  self.next_turn = nil
  self.items = nil
  self.party = nil
  self.party_origin = nil
  self.encounter_origin = nil
  self.enemies = nil
  self.timr = -1
end

function EncounterState:enter(params)
  local atlas = SpriteAtlas()
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 16))
  local n = 0
  self.party_origin = battlefield:east_team_origin()
  self.turns = {}
  self.next_turn = 1
  self.party = {}
  self.enemies = {}
  self.all_entities = {}
  self.timer = -1
  for i, character in ipairs(params.party) do
    if character:get_hp() > 0 then
      local pos = self.party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
      table.insert(self.turns, character)
      table.insert(self.party, character)
      table.insert(self.all_entities, character)
      atlas:add(character, pos, character:get_appearance())
      n = n + 1
    end
  end
  self.encounter_origin = battlefield:west_team_origin()
  for i, character in ipairs(params.encounter) do
    local pos = self.encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[n + i] = character
    table.insert(self.enemies, character)
    table.insert(self.all_entities, character)
    atlas:add(character, pos, character:get_appearance())
  end
  self.items = params.items
  self:view():add('atlas', atlas)
  self:view():add('battlefield', battlefield)
  self:view():add('message', message)
  message:set("You stumble upon an encounter")
end

function EncounterState:leave()
  self:view():get('atlas'):clear()
  self:view():remove('atlas')
  self:view():remove('battlefield')
  self:view():remove('message')
end

function EncounterState:update(dt)
  if self.timer >= 0 then self.timer = self.timer + dt end
  if self.timer >= 2 then self.timer = -1 end
  if self.timer < 0 then
    -- sorts turn order based on speed
    if self:check_game_over() then return self:pop() end

    table.sort(self.turns, function(a,b) return a:get_speed() > b:get_speed() end)
    self.next_turn = math.min(self.next_turn, #self.turns)
    local current_character = self.turns[self.next_turn]
    local params = {
      character = current_character,
      targets = self.all_entities,
      items = current_character:is_enemy() and current_character:get_items() or self.items,
      index = self.next_turn
    }
    self.next_turn = self.next_turn % #self.turns + 1
    if current_character:is_enemy() then return self:push('enemy_turn', params) end
    return self:push('player_turn', params)
  end
end

function EncounterState:check_game_over()
  local allies = self:engine():get_all_alive(true)
  local enemies = self:engine():get_all_alive(false)

  print("Temos " .. #allies .. " aliados vivos")
  print("Temos " .. #enemies .. " inimigos vivos")
  if #allies == 0 or #enemies == 0 then
    print("Fim do encontro")
    return true
  end
end

function EncounterState:update_sprites()
  local atlas = self:view():get('atlas')
  atlas:clear()
  self.turns = {}
  self.party = {}
  self.enemies = {}
  self.all_entities = {}
  local n = 0
  local allies = self:engine():get_all_alive(true)
  local enemies = self:engine():get_all_alive(false)

  for i, character in ipairs(allies) do
    local pos = self.party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[i] = character
    table.insert(self.party, character)
    table.insert(self.all_entities, character)
    atlas:add(character, pos, character:get_appearance())
    n = n + 1
  end
  for i, character in ipairs(enemies) do
    local pos = self.encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[n + i] = character
    table.insert(self.enemies, character)
    table.insert(self.all_entities, character)
    atlas:add(character, pos, character:get_appearance())
  end
end

function EncounterState:resume(params)
  local target = self.all_entities[params.target]
  local message = ''
  local item_id = params.item
  local result = self:engine():random_action(params.character, #self.all_entities)
  local action = params.action
  if result ~= nil then
    action = result[1]
    target = self.all_entities[result[2]]
    item_id = 1
    print("Escolhi aleatoriamente fazer " .. action .. " no " .. target:get_name())
  end
  if action ~= 'Run' then
    if action == 'Fight' then
      result = self:engine():attack(params.character, target)
      if not result then
        result = {'NoFight', 0}
      end
      message = MESSAGES[result[1]]:format(params.character:get_name(),
        target:get_name(), result[2])
      -- if the attack was poisonous, add poison buff
      if result[3] then
        target:add_buff(self:engine():create_effect({ stat = 'hp', value = 2, time = 'infinite' }))
        message = message .. '\n' .. MESSAGES.Poison:format(target:get_name())
      end
    elseif action == 'Item' then
      local item = self.items[item_id]
      result = self:engine():use_item(params.character, item, target)
      if not result then
        result = 'NoItem'
      end
      message = MESSAGES[result]:format(params.character:get_name(),
        item:get_name(), target:get_name(), item:get_message())
    elseif action == 'Skill' then
      local skill = params.character:get_skills()[item_id]
      result = self:engine():use_skill(params.character, skill, target)
      if not result then
        result = 'NoSkill'
      end
      message = MESSAGES[result]:format(params.character:get_name(),
        skill:get_name(), target:get_name(), skill:get_message())
    elseif action == 'Died' then
      message = MESSAGES[action]:format(params.character:get_name())
      self.timer = 0
    end

    if not target then target = params.character end

    -- if an enemy died, get dropped items
    if target:get_hp() == 0 and target:is_enemy() then
      for _,dropped_item in ipairs(target:get_items()) do
        local new_item = true

        for _,item in ipairs(self.items) do
          if item:get_name() == dropped_item:get_name() then
            item:increment_amount(dropped_item:get_amount())
            new_item = false
          end
        end

        if new_item and dropped_item:get_amount() > 0 then
          table.insert(self.items, dropped_item)
        end
      end
    end
  else
    return self:pop()
  end
  self:view():get('message'):set(message)
  self:update_sprites()
end

return EncounterState