
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local ChooseTargetState = require 'common.class' (State)

function ChooseTargetState:_init(stack)
  self:super(stack)
  self.character = nil
  self.action = nil
  self.targets = nil
  self.item = nil
  self.items = nil
  self.menu = nil
end

function ChooseTargetState:enter(params)
  print("entrei no choose target")
  self.character = params.character
  self.targets = params.targets
  self.item = params.item
  self.items = params.items
  self.action = params.action
  self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
end

function ChooseTargetState:_show_menu()
  self.menu = ListMenu(self:get_target_menu(self.targets))
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, ((bfbox.top + bfbox.bottom) / 2) - 32)
  self:view():add('turn_menu', self.menu)
end

function ChooseTargetState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function ChooseTargetState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function ChooseTargetState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function ChooseTargetState:get_target_menu(list)
  self.targets = list
  local names = {}
  for _, e in ipairs(list) do
    table.insert(names, e:get_name())
  end
  return names
end

function ChooseTargetState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    local option = self.menu:current_option()
    return self:pop({ action = self.action, character = self.character, 
      item = self.item, items = self.items, target = option, targets = self.targets})
  elseif key == 'escape' then
    return self:pop( { action = self.action, character = self.character, 
      item = self.item, items = self.items, target = nil, targets = self.targets } )
  end
end

return ChooseTargetState