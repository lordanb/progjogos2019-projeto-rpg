
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local State = require 'state'

local EnemyTurnState = require 'common.class' (State)


function EnemyTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.items = nil
  self.action = nil
  self.targets = nil
  self.party = nil
  self.timer = 0
  self.index = 0
end

function EnemyTurnState:enter(params)
  print("entrei no enemy turn")
  self.character = params.character
  self.character:check_buffs()
  self.character:check_status()
  self.action = nil
  self.targets = self.character:get_targets()
  self.party = params.targets
  self.items = params.items
  self.index = params.index
  self:_show_cursor()
  self:_show_stats()
end

function EnemyTurnState:_get_target_index()
  if self.targets then
    for _,target in pairs(self.targets) do
      for i,character in ipairs(self.party) do
        if target == character.get_name() then return i end
      end
    end
  end

  return 1
end

function EnemyTurnState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function EnemyTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function EnemyTurnState:leave()
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function EnemyTurnState:resume(params)
  print("voltei pro enemy turn")
  if params.item or params.action == 'Run' or (params.action == 'Fight' and params.target) then
    return self:pop(params)
  end
  self:enter(params)
end

function EnemyTurnState:update(dt)
  self.timer = self.timer + dt

  if self.timer >= 2 then
    self.timer = 0
    -- if enemy is dying, change behaviour
    if self.character:get_hp() <= self.character:get_max_hp()*.3 then
      if self.character:get_critical_behaviour() == 'item' then
        return self:pop({ action = 'Item', character = self.character, 
          item = 1, items = self.items, target = self.index, targets = self.party})
      end
    end

    local index = self:_get_target_index()
    print(self.character:get_name() .. ' escolheu atacar ' .. self.party[index]:get_name())

    return self:pop({ action = 'Fight', character = self.character, 
      item = nil, items = self.items, target = index, targets = self.party})
  end
end

return EnemyTurnState

